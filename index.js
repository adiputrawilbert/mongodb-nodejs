const MongoClient = require('mongodb').MongoClient;

const express = require('express')

var bodyParser = require('body-parser')

// create application/json parser
var jsonParser = bodyParser.json()

let app = express()
const PORT_NUMBER = 3000

const server = app.listen(PORT_NUMBER, ()=>{
    console.log("Server is running on port " + PORT_NUMBER)
})

MongoClient.connect('mongodb://localhost:27017/',

    { useUnifiedTopology: true } ,(err, client)=>{

        if(err) throw (err)
        console.log("connection to database established")

        const db = client.db('Quiz1')

        app.route("/books")
            .get((req,res)=>{
                db.collection("books")
                    .find({})
                    .project({category : 1, title : 1, _id : 0})
                    .limit(3)
                    .toArray(function(err2, result){
                        if(err2) throw (err2)
                        res.send(result)
                    })
            })


        app.route("/books/:language")
            .get((req,res)=>{

                let languageFromParams = req.params.language
                let languageFromGet = req.query.language

                db.collection("books")
                    .find({language : languageFromParams})
                    .project({category : 1, title : 1, _id : 0})
                    .limit(3)
                    .toArray(function(err2, result){
                        if(err2) throw (err2)
                        res.send(result)
                    })

            })

        app.route("/books/:language")
            .post(jsonParser, (req,res)=>{

                let languageFromParams = req.params.language
                // let languageFromGet = req.query.language
                let languageFromPostPutDeleteEtc = req.body.language

                res.send({
                    languageFromParams : languageFromParams,
                    // languageFromGet : languageFromGet,
                    languageFromPostPutDeleteEtc : languageFromPostPutDeleteEtc
                })

            })

        //UPDATE books SET author = "pak maula" WHERE title = "new book4"

        /*
        db.collection("books").deleteOne({
            title : "new book4",
        },function(err2, result){
            if(err2) throw (err2)
            console.log(result)
        })
        */

        /*
        db.collection("books").updateOne({
            title : "new book4",
        }, {
            $set : {author : "pak maula"},
        },function(err2, result){
            if(err2) throw (err2)
            console.log(result)
        })
        */

        /*Insert Many
        db.collection("books").insertMany([{
            title : "new book4",
            author : "me4",
        }], function(err2, result){
            if(err2) throw (err2)
            console.log(result)
        })
        */

        /* Insert One
        db.collection("books").insertOne({
            title : "new book",
            author : "me",
        }, function(err2, result){
            if(err2) throw (err2)
            console.log(result)
        })*/

        /* Projection
        db.collection("books")
            .find({})
            .project({category : 1, title : 1, _id : 0})
            .limit(3)
            .toArray(function(err2, result){
                if(err2) throw (err2)
                console.log(result)
            })*/

    })
